# homebridge-tvservice-braviakdl

Homebridge iOS 12.2 Tvservice Plugin for Homebridge and old Sony Bravia KDL TVs

This code is just a Proof of Concept, feel free to make changes!


## Installation
1. Install homebridge using: npm install -g homebridge
2. Install this plugin using: 
    git clone https://gitlab.com/gotschi/homebridge-tvservice-braviakdl && cd homebridge-tvservice-braviakdl
    sudo npm -g install --unsafe-perm ./


### Configure config.json
Example config:

```
"accsessories":[
  {
        "accessory": "tvservice-bravia",
        "name": "Bravia",
        "ip": "10.0.0.2",
        "port": "80",
        "mac": "00:00:00:00:00:00",
        "psk": "psk"
    }
]
```



Ressources:

https://www.reddit.com/r/shortcuts/comments/9oh4zh/control_sony_tv_power_offon_and_other_tv_commands/

https://www.reddit.com/r/shortcuts/comments/9kur6d/control_your_sony_android_tv/e7gzd8i/

https://github.com/merdok/homebridge-webos-tv/blob/master/index.js

https://github.com/KhaosT/HAP-NodeJS/blob/master/lib/gen/HomeKitTypes-Television.js

https://pro-bravia.sony.net/develop/integrate/ircc-ip/ircc-codes/